"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    #Vom Vektor mit den Einträgen der Beschleunigung in x, y und z Richtung wird der Betrag genommen um den absoluten Wert zu bekommen
    
    #Das Quadrat der einzelnen Beschleunigungen berechnen
    acc_x_sq = np.square(x)
    acc_y_sq = np.square(y)
    acc_z_sq = np.square(z)
    
    #Betrag berechnen
    absolute_value_acceleration = np.sqrt(acc_x_sq+acc_y_sq+acc_z_sq)
   
    return absolute_value_acceleration

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    #Neue Zeitwerte erstellen, welche gleichmäßig zwischen kleinstem und größten Zeitwert verteilt sind
    evenly_distributed_time = np.linspace(np.min(time), np.max(time), len(time))
    
    #Interpolation der Daten, also zu den neuen Zeitwerten werden nun interpolierte Datenwerte erstellt 
    interpolation_data = np.interp(evenly_distributed_time, time, data)

    return (evenly_distributed_time, interpolation_data)

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    #Mittelwert von dem input x (measurement data) berechnen
    mean_measurement_data = np.mean(x)
    
    #Messdaten um den Mittelwert verschieben (ihn subtrahieren)
    x_adjusted = x - mean_measurement_data
    
    #Die Fast-Fourier-Transformation bei den angepassten Daten durchführen mit gegebener Funktion
    FFT_measurement_data = np.fft.fft(x_adjusted)
    
    #um FFT durchzuführen muss man die Anzal der Datenpunkte auslesen und wissen (mit len)
    array_length = len(x_adjusted)
    
    #zeitlichen Abstand zwischen den Messungen ausreechenen
    time_between_measurements = time[1]-time[0]
    
    #Frequenz berechnen
    frequency = np.fft.fftfreq(array_length, time_between_measurements)

    #nur positive Frequenzen relevant (sinnvoll)
    f = frequency >=0
    pos_frequency = frequency[f]
    
    #die Amplitude berechnen
    FFT_measurement_data_pos = FFT_measurement_data[f]
    pos_amplitude = np.abs(FFT_measurement_data_pos)
    return(pos_amplitude, pos_frequency)
    