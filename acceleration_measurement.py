import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_handy.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Die UUID des Sensors aus dem im vorherigen Code erstellten sensor settings dict auslesen
uuid_acceleration_sensor = sensor_settings_dict["ID"]
#Die leeren Listen erstellen
list_1 = []
list_2 = []
list_3 = []
list_4 =[]
#Dictionary erstellen mit UUID als Schlüssel und mit den vier (noch leeren) Listen als Inhalt
measurement_data_dict = {uuid_acceleration_sensor:{"acceleration_x":list_1,"acceleration_y":list_2,"acceleration_z":list_3,"timestamp":list_4}}
# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Messaufnahme programmieren mit Hilfe einer Schleife (measure_duration_in_s oben im Code auf 20 festgelegt)
i2c = board.I2C()
accelerometer = adafruit_adxl34x.ADXL345(i2c)
start_time = time.time()

while time.time()-start_time<measure_duration_in_s:
    
    #accceleration in x-Richtung
    acceleration_x = accelerometer.acceleration[0]
    #in y-Richtung
    acceleration_y = accelerometer.acceleration[1]
    #in z-Richtung
    acceleration_z = accelerometer.acceleration[2]
    #timestamp
    timestamp = time.time()-start_time

    #Listen im dictionary füllen mit den Messdaten
    measurement_data_dict[uuid_acceleration_sensor]["acceleration_x"].append(acceleration_x)
    measurement_data_dict[uuid_acceleration_sensor]["acceleration_y"].append(acceleration_y)
    measurement_data_dict[uuid_acceleration_sensor]["acceleration_z"].append(acceleration_z)
    measurement_data_dict[uuid_acceleration_sensor]["timestamp"].append(timestamp)

    time.sleep(0.001)
# ---------------------------------------------------------------------------------------------#3-end
# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
with h5py.File(path_h5_file, "w") as file:
    #Gruppe RawData erstellen
    group_raw = file.create_group("RawData")
    
    #Gruppe erstellen mit der Sensor UUID
    uuid_group = group_raw.create_group(uuid_acceleration_sensor)
    
    #Attribut erstellen mit den jeweiligen Einheiten
    unit = ["m/s²","m/s²","m/s²","s"]
    
    #for Schleife welche die Messdaten in der HDF5-Datei speichert und noch unit als Attribute hinzufügt
    i=0
    for key in measurement_data_dict[uuid_acceleration_sensor].keys():
        dataset = uuid_group.create_dataset(key, data = measurement_data_dict[uuid_acceleration_sensor][key])
        dataset.attrs["unit"] = unit[i]
        i+=1
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
